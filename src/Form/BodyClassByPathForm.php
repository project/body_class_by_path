<?php
/**
 * @file
 * Contains \Drupal\body_class_by_path\Form\BodyClassByPathForm.
 */
namespace Drupal\body_class_by_path\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

class BodyClassByPathForm extends FormBase {
  /**
   * {@ define form get id function for form id declairation}
   */
  public function getFormId() {
    return 'body_class_by_path_form';
  }

  /**
   * {@ define form buil function for form creation}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['name'] = array(
		'#title' => 'Name',
		'#description' => 'Type name here.',
		'#type' => 'textfield',
		'#required' => TRUE,
	);
	$form['path'] = array(
		'#title' => 'Path',
		'#description' => 'Enter a full url of the any page.',
		'#type' => 'textfield',
		'#required' => TRUE,
	);
	$form['classname'] = array(
		'#title' => 'Class Name',
		'#description' => 'Enter class name as you want.',
		'#type' => 'textfield',
		'#required' => TRUE,
	);
	$form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    );	
	return $form;
  }

  /**
   * {@ submitform function for inserting data in table using services}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
	\Drupal::service('body_class_by_path.data_handler')->setData($form_state);
	global $base_url;
	$response = new \Symfony\Component\HttpFoundation\RedirectResponse($base_url ."/admin/config/pathlist");
	$response->send();
   }
}