<?php
/**
 * @file
 * Contains \Drupal\body_class_by_path\Form\BodyClassByPathEditForm.
 */
namespace Drupal\body_class_by_path\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\RedirectResponse;

class BodyClassByPathEditForm extends FormBase {
  /**
   * {@ define form get id function for form id declairation}
   */
  public function getFormId() {
    return 'body_class_by_path_edit_form';
  }

  /**
   * {@ define form buil function for form creation}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
	
	$path = \Drupal::service('path.current')->getPath();
    $path_args = explode('/', $path);
	$pid = $path_args[3];
	
	$query = \Drupal::database()->select('body_class_by_path', 'bcp');
	$query->condition('bcp.id', $pid, '=');
	$query->fields('bcp', ['id', 'name', 'path', 'classname']);
	$result = $query->execute()->fetchAll();
	
	//print_r($result);
    $form['name'] = array(
		'#title' => 'Name',
		'#description' => 'Type name here.',
		'#type' => 'textfield',
		'#default_value'=> $result[0]->name,
		'#required' => TRUE,
	);
	$form['path'] = array(
		'#title' => 'Path',
		'#description' => 'Enter a full url of the any page.',
		'#type' => 'textfield',
		'#default_value'=> $result[0]->path,
		'#required' => TRUE,
	);
	$form['classname'] = array(
		'#title' => 'Class Name',
		'#description' => 'Enter class name as you want.',
		'#type' => 'textfield',
		'#default_value'=> $result[0]->classname,
		'#required' => TRUE,
	);
	$form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = array(
      '#type' => 'submit',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    );	
	return $form;
  }
  
  /**
   * {@ submitform function for inserting data in table using services}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
	
	$path = \Drupal::service('path.current')->getPath();
    $path_args = explode('/', $path);
	$pid = $path_args[3];	
	\Drupal::service('body_class_by_path.data_handler')->updateData($form_state, $pid);	
	global $base_url;	
	$response = new \Symfony\Component\HttpFoundation\RedirectResponse($base_url ."/admin/config/pathlist");
	$response->send();
	
   }
}