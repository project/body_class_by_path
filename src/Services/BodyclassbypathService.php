<?php

namespace Drupal\body_class_by_path\Services;
use Drupal\Core\Database\Connection;
/**
 * Service class BodyclassbypathService.
 */
class BodyclassbypathService {
  /**
   * @var \Drupal\Core\Database\Connection $database
   */
  protected $database;

  /**
   * Constructs a new BodyclassbypathService object.
   * @param \Drupal\Core\Database\Connection $connection
   */
  public function __construct(Connection $connection) {
    $this->database = $connection;
  }

  /**
   * @Inserting data in table using services.
   */
    public function setData($form_state){
		$this->database->insert('body_class_by_path')
		 ->fields([
			'name',
			'path',
			'classname',		 
		 ])
		 ->values(array(
			$form_state->getValue('name'),
			$form_state->getValue('path'),
			$form_state->getValue('classname'),
			
		 ))
		 ->execute();		 
	}
	
	/**
    * @Updating data in table using services.
    */
    public function updateData($form_state, $pid){
		$this->database->update('body_class_by_path')
		 ->fields([
			'name' => $form_state->getValue('name'),
			'path' => $form_state->getValue('path'),
			'classname' => $form_state->getValue('classname'),		 
		 ])
		 ->condition('id', $pid, '=')
		 ->execute();
		 
	}
	
	/**
    * @Deleting data from record using services.
    */
	public function deleteData($pid){
		$this->database->delete('body_class_by_path')
		 ->condition('id', $pid, '=')
		 ->execute();		 
	}
}