<?php

/**
 * @return
 * Contains \Drupal\body_class_by_path\Controller\BodyClassByPathList.
 */
 
namespace Drupal\body_class_by_path\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
 
/**
 * Provides route responses for the BodyClassByPathList module.
 */
class BodyClassByPathList {
 
  /**
   * Get all data from the body_class_by_path table
   *   
   */
  public function pathslist() {
	  
	$query = \Drupal::database()->select('body_class_by_path', 'bcp');
	$query->fields('bcp', ['id', 'name', 'path', 'classname']);
	$result = $query->execute()->fetchAll();
	$listtable .='<table><tr>
    <th>Sr.No.</th>
	<th>Name</th>
    <th>Path</th>
    <th>Class Name</th>
    <th>Actions</th>
  </tr>';
    $i=1;
    foreach($result as $pathresult){
		global $base_url;
		$id = $pathresult->id;
		$name = $pathresult->name;
		$path = $pathresult->path;
		$classname = $pathresult->classname;
	$listtable .='<tr>
	<td>'.$i.'</td>
    <td>'.$name.'</td>
    <td>'.$path.'</td>
    <td>'.$classname.'</td>
    <td><a href="'.$base_url.'/admin/config/'.$id.'/editpath" title="Generate Receipt">Edit</a>|<a href="'.$base_url.'/admin/config/'.$id.'/deletepath" title="Generate Receipt">Delete</a></td>
  </tr>';
    $i++;
	}
	$listtable .='</table>';
    $element = array(
      '#markup' => $listtable,
    );
    return $element;
  }
  
  /**
  *@ deletepath function for performing to the delete action.
  */
  public function deletepath() {
	 $path = \Drupal::service('path.current')->getPath();
    $path_args = explode('/', $path);
	$pid = $path_args[3];
	
	\Drupal::service('body_class_by_path.data_handler')->deleteData($pid);
	global $base_url;
	$response = new \Symfony\Component\HttpFoundation\RedirectResponse($base_url ."/admin/config/pathlist");
	$response->send();
  }
 
}
