CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

Body Class By Path module is use to add the class for specific page.
Going to the configuration page under developement section there are two
links is showing one is for adding classes based on putting url and other
is for showing the list of all classes.

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/body_class_by_path


REQUIREMENTS
------------

No special requirements.


RECOMMENDED MODULES
-------------------

No recommendetion.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/node/895232 for further information.


CONFIGURATION
-------------

No special configuration.


MAINTAINERS
-----------

Current maintainers:
 * Ajay Kumar Bhaskar (ajaybhaskar) - https://www.drupal.org/u/ajaybhaskar